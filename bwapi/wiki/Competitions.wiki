#summary Competitions held for BWAPI
#labels Featured,Deprecated

= *Competitions are no longer tracked by BWAPI maintainers* =


This page contains a list of competitions using BWAPI.

_Note: In most cases you do not need to be present at the tournament location, so you can still submit an entry!_

<wiki:toc max_depth="2" />

----
= In Progress =
 == 2013 ==
  * [http://scmai.hackcraft.sk/?lang=en Starcraft Micro AI Tournament 2] (July 5th - July 7th)
  * [http://ls11-www.cs.uni-dortmund.de/rts-competition/starcraft-cig2013 CIG 2013] (July 19th - August 13th)
  * [http://webdocs.cs.ualberta.ca/~cdavid/starcraftaicomp/index.shtml AIIDE 2013] (July)
  * [http://www.sscaitournament.com/ SSCAI Tournament 2013] (December)
  
----
= Finished =
 == 2013 ==
  * [http://www.hackcraft.sk/article/default/starcraft-micro-ai-tournament-2013-report Starcraft Micro AI Tournament] (February 15th - February 17th)

 == 2012 ==
  * [http://code.google.com/p/bwapi/wiki/BWAPICodeTournament BWAPI Winter Tournament 2012] (January 7th - January 8th)
  * [http://webdocs.cs.ualberta.ca/~cdavid/starcraftaicomp/media.shtml AIIDE 2012]
  * [http://ls11-www.cs.uni-dortmund.de/rts-competition/starcraft-cig2012 CIG 2012] (August 15th - September 15th)
  * [http://www.sscaitournament.com/index.php?action=2012 SSCAI Tournament 2012] (December)

 == 2011 ==
  * [https://skatgame.net/mburo/sc2011/ AIIDE 2011] (July 1st - August 19th)
  * [http://www.broodwarai.com/forums/viewtopic.php?f=7&t=901 All-Open ICCUP Bot Tournament] (July 3rd)
  * [http://ls11-www.cs.uni-dortmund.de/rts-competition/starcraft-cig2011 CIG 2011] (August 15th - September 3rd)
  * [http://www.sscaitournament.com/index.php?action=2011 SSCAI Tournament 2011] (December)

 == 2010 ==
  * [http://ls11-www.cs.uni-dortmund.de/rts-competition/starcraft-cig2010 CIG 2010] (June 30th - August 21st)
  * [http://eis.ucsc.edu/StarCraftAICompetition AIIDE 2010] (September 1st - October 15th)