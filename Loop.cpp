#include <iostream>

using namespace std; // So i can use cout, cin and endl

int main() {
	// I want to reach a hundred, then zero using FOR, WHILE and DO..WHILE
	int numero; // Here i will save the first number
	cout << "Please, give me a number: "; // Please, please, please
	cin >> numero; // This is the base number to start
	for ( numero; numero < 100; numero++ ); // Here, we try to reach the sky, i mean, 100
	cout << "And we did it !" << endl;
	cout << "The number become: " << numero << endl;
}
